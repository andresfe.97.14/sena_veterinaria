<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/users', [App\Http\Controllers\UserController::class, 'index'])->name('users.index');
Route::post('/newUsers', [App\Http\Controllers\UserController::class, 'newUser'])->name('users.new');
Route::post('/getUserInfo', [App\Http\Controllers\UserController::class, 'getUserInfo'])->name('users.info');
Route::post('/editUser', [App\Http\Controllers\UserController::class, 'editUser'])->name('users.edit');
Route::post('/delete', [App\Http\Controllers\UserController::class, 'delete'])->name('users.delete');
Route::get('/dataUsers', [App\Http\Controllers\UserController::class, 'get_users'])->name('users.get');

Route::get('/pets', [App\Http\Controllers\PetController::class, 'index'])->name('pets.index');
Route::post('/newPet', [App\Http\Controllers\PetController::class, 'new'])->name('pets.new');
Route::post('/getPet', [App\Http\Controllers\PetController::class, 'getPet'])->name('pet.info');
Route::post('/editPet', [App\Http\Controllers\PetController::class, 'editPet'])->name('pet.edit');
Route::post('/deletePet', [App\Http\Controllers\PetController::class, 'delete'])->name('pet.delete');
Route::get('/datapets', [App\Http\Controllers\PetController::class, 'getTable'])->name('pet.table');


Route::get('/campus', [App\Http\Controllers\CampusController::class, 'index'])->name('campus.index');
Route::post('/newCampus', [App\Http\Controllers\CampusController::class, 'new'])->name('campus.new');
Route::post('/getCampus', [App\Http\Controllers\CampusController::class, 'getCampusInfo'])->name('campus.info');
Route::post('/editCampus', [App\Http\Controllers\CampusController::class, 'editCampus'])->name('campus.edit');
Route::post('/deleteCampus', [App\Http\Controllers\CampusController::class, 'delete'])->name('campus.delete');
Route::get('/datacampus', [App\Http\Controllers\CampusController::class, 'getTable'])->name('campus.table');

Route::get('/appointment', [App\Http\Controllers\AppointmentController::class, 'index'])->name('appointment.index');
Route::post('/newAppointment', [App\Http\Controllers\AppointmentController::class, 'new'])->name('appointment.new');
Route::post('/getAppointment', [App\Http\Controllers\AppointmentController::class, 'getAppointment'])->name('appointment.info');
Route::post('/editAppointment', [App\Http\Controllers\AppointmentController::class, 'editAppointment'])->name('appointment.edit');
Route::post('/deleteAppointment', [App\Http\Controllers\AppointmentController::class, 'delete'])->name('appointment.delete');
Route::post('/finishappointment', [App\Http\Controllers\AppointmentController::class, 'finish'])->name('appointment.finish');
Route::get('/dataAppointment', [App\Http\Controllers\AppointmentController::class, 'getTable'])->name('appointment.get');
Route::get('/appointmentone/{appointment}', [App\Http\Controllers\AppointmentController::class, 'appointmentNow'])->name('appointment.one');

Route::get('/UserProfile', [App\Http\Controllers\UserController::class, 'userProfile'])->name('user.profile');
Route::get('/UserPet', [App\Http\Controllers\UserController::class, 'userPet'])->name('user.pets');
Route::post('/createPetUser', [App\Http\Controllers\PetController::class, 'createPetUser'])->name('pet.create');
Route::get('/petbyappointmen/{pet}',[App\Http\Controllers\AppointmentController::class, 'reviewAppointmentPet'])->name('pet.appointments');

Route::get('/doctor/home', [App\Http\Controllers\DoctorController::class, 'index'])->name('doctor.index');
Route::get('/doctor/appointmentnow/{pet}/{appointment}', [App\Http\Controllers\DoctorController::class, 'appointmentNow'])->name('doctor.now');
// Route::get('/UserPet', [App\Http\Controllers\UserController::class, 'userPet'])->name('user.pets');
// Route::post('/createPetUser', [App\Http\Controllers\PetController::class, 'createPetUser'])->name('pet.create');