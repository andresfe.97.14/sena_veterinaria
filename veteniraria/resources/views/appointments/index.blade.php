@extends('layouts.app')

@section('css')
<link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
@endsection

@section('content')
<div id='testt' class="container">

    <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
        Nuevo+
    </button>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Citas') }}</div>

                <div class="card-body">
                    

                    <div class="table-responsive">
                    <table class="table table-striped dt-responsive table-sm nowrap" style="width:100%" id="appointments-table">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Fecha</th>
                                <th>Hora</th>
                                <th>Tipo</th>
                                <th>Observacion</th>
                                <th>Estado</th>
                                <th>Doctor</th>
                                <th>Mascota</th>
                                <th>Sede</th>
                              
                                <th class="text-center">Acciones</th>

                            </tr>
                        </thead>
                     
                    </table>
                </div>



                </div>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Crear nueva Cita</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form id="form-register">
                @csrf
                
                    <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">Fecha</label>
                        <label for="exampleInputEmail1" class="form-label">Fecha</label>
                        <input type="date" class="form-control" id="inputdate" name="inputdate" aria-describedby="emailHelp" required>
                        <div id="emailHelp" class="form-text"></div>
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">Hora</label>
                        <select name="inputhour" id="inputhour" class="form-control" required>
                            <option value="" selected disabled>Seleccione una opcion </option>
                            <option value="8">8am</option>
                            <option value="9">9am</option>
                            <option value="10">10am</option>
                            <option value="11">11am</option>
                            <option value="12">12pm</option>
                            <option value="13">13pm</option>
                            <option value="14">14pm</option>
                            <option value="15">15pm</option>
                            <option value="16">16pm</option>
                            <option value="17">17pm</option>
                        </select>
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">Tipo</label>
                        <input type="text" class="form-control" id="inputtype" name="inputtype" aria-describedby="emailHelp" required>
                    </div>

                    <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">Observaciones</label>
                        <input type="text" class="form-control" id="inputobservation" name="inputobservation" aria-describedby="emailHelp" required>
                    </div>

                    <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">Doctor</label>
                        <select name="inputdoctorid" class="form-control" id="inputdoctorid">
                            <option value="" disabled selected> Seleccione un doctor</option>
                        @foreach($users_doctor as $user)
                            <option value="{{$user->id}}"> {{$user->name}}</option>
                        @endforeach
                        </select>
                     
                    </div>

                    <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">Mascota</label>
                        <select name="inputpetid"  class="form-control" id="inputpetid">
                            <option value=""  selected disabled >Selecciona una mascota</option>
                            @foreach($pets as $pet)
                                <option value="{{$pet->id}}">{{$pet->name}}</option>
                            @endforeach
                        </select>
                    
                    </div>

                    <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">Campus</label>
                        <select name="inputcampusid" id="inputcampusid" class="form-control">
                            <option value="" selected disabled>Seleccione una sede </option>
                            @foreach($campuses as $campus)
                                 <option value="{{$campus->id}}"> {{$campus->name}}</option>
                            @endforeach

                        </select>
                       
                    </div>
                    
                   
               
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="exampleModal2" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form id="form-edit">
                @csrf   
                    <input type="text" id="editid" name="editid" hidden required>
                    @csrf
                <div class="mb-3">
                        <label for="exampleInputPassword1" class="form-label">Razon</label>
                        <input type="text" class="form-control" name="editname"id="editname" required>
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">Fecha</label>
                        <input type="date" class="form-control" id="editdate" name="editdate" aria-describedby="emailHelp" required>
                        <div id="emailHelp" class="form-text"></div>
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">Hora</label>
                        <input type="text" class="form-control" id="edithour" name="edithour" aria-describedby="emailHelp" required>
                    </div>

                    <div class="mb-3" id=editdoctordiv>
                        <label for="exampleInputEmail1" class="form-label">Doctor</label>
                        <select name="editdoctorid" class="form-control" id="editdoctorid">
                            <option value="" disabled selected> Seleccione un doctor</option>
                        @foreach($users_doctor as $user)
                            <option value="{{$user->id}}"> {{$user->name}}</option>
                        @endforeach
                        </select>
                     
                    </div>

                    <div class="mb-3" id=editpetid_div>
                        <label for="exampleInputEmail1" class="form-label">Mascota</label>
                        <select name="editpetid"  class="form-control" id="editpetid">
                            <option value=""  selected disabled >Selecciona una mascota</option>
                            @foreach($pets as $pet)
                                <option value="{{$pet->id}}">{{$pet->name}}</option>
                            @endforeach
                        </select>
                    
                    </div>

                    <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">Campus</label>
                        <select name="editcampusid" id="editcampusid" class="form-control">
                            <option value="" selected disabled>Seleccione una sede </option>
                            @foreach($campuses as $campus)
                                 <option value="{{$campus->id}}"> {{$campus->name}}</option>
                            @endforeach

                        </select>
                       
                    </div>

                   
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>


<!-- jQuery -->
<script src="//code.jquery.com/jquery.js"></script>
        <!-- DataTables -->
        <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
        <!-- Bootstrap JavaScript -->
        <script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

<script>
     $(function() {
            
           let table = $('#appointments-table').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                fixedColumns: true,
                ajax: '{!! route('appointment.get') !!}',
               
                columns: [{
                        data: 'id',
                        name: 'id',
                        render: function (  data, type, row  ) {
                            return row.id;
                        }
                    },
                    {
                        data: 'date',
                        name: 'date',
                        render: function (  data, type, row  ) {
                            return row.date;
                        }
                    },
                    {
                        data: 'hour',
                        name: 'hour',
                        render: function (  data, type, row  ) {
                            return row.hour;
                        }
                    },
                    {
                        data: 'type',
                        name: 'type',
                        render: function (  data, type, row  ) {
                            return row.type;
                        }
                    },
                    {
                        data: 'observation',
                        name: 'observation',
                        render: function (  data, type, row  ) {
                            return row.observation;
                        }
                    },
                    {
                        data: 'state',
                        name: 'state',
                        render: function (  data, type, row  ) {
                            return row.state;
                        }
                        
                    },
                    {
                        data: 'doctor',
                        name: 'dcotor',
                        render: function (  data, type, row  ) {
                            return row.doctor.name;
                        }
                        
                    },
                    {
                        data: 'pet',
                        name: 'pet',
                        render: function (  data, type, row  ) {
                            return row.pet.name;
                        }
                        
                    },
                    {
                        data: 'campus',
                        name: 'campus',
                        render: function (  data, type, row  ) {
                            return row.campus.name;
                        }
                        
                    },
                

                   
                    {
                        data: null,
                        name: 'actions',
                        searchable: 'id',
                        orderable: 'id',
                        render: function (  data, type, row,index  ) {
                       
                          
                            return `
                              
                                    <button class="btn btn-danger " onclick='deleteAppointment(${row.id})'  >
                                        <i class="fas fa-times"></i>Borrar
                                    </button>
                                    <button class="btn btn-success " onclick="editAppointment(${row.id})"  data-bs-toggle="modal" data-bs-target="#exampleModal2">
                                        <i class="fas fa-pencil-alt"></i>Editar
                                    </button>
               
                            `;
                        }
                    },
                ]
            });

            // s
        });




    $("#form-register").submit(function(event) {
        event.preventDefault();
        let formData = new FormData(document.getElementById("form-register"));
  
       
        $.ajax({
                type: "POST",
                url: "{!! route('appointment.new') !!}",
                data: formData,
                processData: false,
                contentType: false,
                success: (response) => {
                    alert("Campus creado con exito");
                    location.reload();
                    if (response == 201) {
                        
                   
                    }
                },
                error: (err) => {
                    alert('fallo');
                }

    });

});

 function editAppointment(id){
    
    console.log(id)
    alert(id);
    let url = "{!! route('appointment.one', 'id') !!}";
                // url = url.replace(':diligence', diligence);
                // url = url.replace(':client', client);
                // url = url.replace(':nit', nit);
                url = url.replace('id', id);
                location.href = url;





    alert('paren todo');

    
    let formData = new FormData();
            formData.append("_token", "{!! csrf_token() !!}");
            formData.append("id", id);

    $.ajax({
                type: "POST",
                url: "{!! route('appointment.info') !!}",
                data: formData,
                processData: false,
                contentType: false,
                success: (response) => {
                    console.log(response);
                    console.log(response.apointments)
                    console.log('lllllllll');
                    console.log(response.apointments.doctor_id);
                    console.log(response.doctors);
                    alert("Usuario creado con exito1");
                    document.getElementById('editname').value = response.name;
                    document.getElementById('editid').value = id;
                    document.getElementById('editdate').value = (response.apointments.date).toISOString().substring(0,10);;    
                    


                    document.getElementById('edithour').value = response.hour;     
                    editpetid_div
                    let editordiv = document.getElementById('editdoctordiv')

                    let doctor_select =`<label for="exampleInputEmail1" class="form-label">Doctor</label>
                    <select name="editdoctorid" class='form-control'id="editdoctorid">`;
                    doctor_select += `<option disabled> Seleccione un medico</option>`;
                    response.doctors.forEach(function (doctor, index) {
                        console.log('qqqqqqq');
                        if(doctor.id ==  response.apointments.doctor_id){
                            console.log('entraaaa');
                            doctor_select += `
                                
                                <option  value="${doctor.id}" selected > ${doctor.name} </option> `;        
                        }else{
                            doctor_select += `<option value="${doctor.id}"   >${doctor.name} </option> `;
                        }
                        
                        console.log('holalla');
                        console.log(doctor);



                    
                    });

                    doctor_select +=`</select>`
                    editordiv.innerHTML = doctor_select;


                    
                    let pet_editordiv = document.getElementById('editpetid_div')

                    let pet_select =`<label for="exampleInputEmail1" class="form-label">Mascota</label>
                    <select name="editpetid" class='form-control'id="editpetid">`;
                    pet_select += `<option disabled> Seleccione una mascota</option>`;
                    response.pets.forEach(function (pet, index) {
                        console.log('qqqqqqq');
                        if( pet.id ==  response.apointments.pet_id){
                            console.log('entraaaa');
                            pet_select += `
                                
                                <option  value="${pet.id}" selected > ${pet.name} </option> `;        
                        }else{
                            pet_select += `<option value="${pet.id}"   >${pet.name} </option> `;
                        }
                        
                        console.log('holalla');
                        



                    
                    });

                    pet_select +=`</select>`
                    pet_editordiv.innerHTML = pet_select;

                    document.getElementById('editdoctorid').value = response.doctor_id;                 
                    document.getElementById('editpetid').value = response.pet_id;     
                    document.getElementById('editcampusid').value = response.campus_id;       
                },
                error: (err) => {
                    alert('fallo');
                }

    });
 }

 $("#form-edit").submit(function(event) {
        event.preventDefault();
        let formData = new FormData(document.getElementById("form-edit"));
        $.ajax({
                type: "POST",
                url: "{!! route('appointment.edit') !!}",
                data: formData,
                processData: false,
                contentType: false,
                success: (response) => {
                    alert("Campus editado con exito");
                    location.reload();
                },
                error: (err) => {
                    alert('fallo');
                }
    });
});

function deleteAppointment(id){
    
    let formData = new FormData();
            formData.append("_token", "{!! csrf_token() !!}");
            formData.append("id", id);
    $.ajax({
                type: "POST",
                url: "{!! route('appointment.delete') !!}",
                data: formData,
                processData: false,
                contentType: false,
                success: (response) => {
                    console.log(response);
                    alert("Campus eliminado con exito");
                     location.reload();
                },
                error: (err) => {
                    alert('fallo');
                }

    });
 }


</script>
@endsection
@section('scripts')

@endsection