@extends('layouts.app')

@section('content')

<div class="container rounded bg-white mt-5 mb-5" style="background-color:rgba(0, 0, 0, 0.5);">
    <div class="row">
      
        <div class="col-md-12 border-right">
            <div class="p-3 py-5">
                <div class="d-flex justify-content-between align-items-center mb-3">
                    <h4 class="text-right">Citacion</h4>
                </div>
                <form id="form-edit">
                @csrf
                    <input type="text" name="editid" id="editid" value="{{$appointment_now->id}}" hidden>
                    <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">Fecha</label>
                        <input type="text" class="form-control" id="editdate" name="editdate" placeholder="{{date('d-m-Y', strtotime($appointment_now->date))}}" value="{{date('d-m-Y', strtotime($appointment_now->date))}}"  required>
                        <div id="emailHelp" class="form-text"></div>
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">Hora</label>
                        <select name="edithour" id="edithour" class="form-control" required>
                        <option value=""  disabled>Seleccione una opcion </option>
                            @for($i=8; $i<=17; $i++)           
                            <option value="{{$i}}" @if($i == $appointment_now->hour)selected @endif  >
                                    @if($i<=11)
                                    {{$i.'am'}}
                                    @else{{$i.'pm'}}
                                    @endif
                            </option>
                            @endfor
                        </select>
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">Tipo</label>
                        <input type="text" class="form-control" id="edittype" name="edittype" value="{{$appointment_now->type}}" required>
                    </div>

                    <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">Observaciones</label>
                        <input type="text" class="form-control" id="editobservation" name="editobservation" value="{{$appointment_now->observation}}" required>
                    </div>

                    <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">Doctor</label>
                        <select name="editdoctorid" class="form-control" id="editdoctorid">
                            <option value="" disabled selected> Seleccione un doctor</option>
                        @foreach($users_doctor as $user)
                            <option value="{{$user->id}}" @if($appointment_now->doctor_id == $user->id)selected @endif > {{$user->name}}</option>
                        @endforeach
                        </select>
                     
                    </div>

                    <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">Mascota</label>
                        <select name="editpetid"  class="form-control" id="editpetid">
                            <option value=""  selected disabled >Selecciona una mascota</option>
                            @foreach($pets as $pet)
                                <option value="{{$pet->id}}" @if($pet->id == $appointment_now->pet_id) selected @endif >{{$pet->name}}</option>
                            @endforeach
                        </select>
                    
                    </div>

                    <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">Campus</label>
                        <select name="editcampusid" id="editcampusid" class="form-control">
                            <option value="" selected disabled>Seleccione una sede </option>
                            @foreach($campuses as $campus)
                                 <option value="{{$campus->id}}" @if($campus->id == $appointment_now->campus_id) selected @endif  > {{$campus->name}}</option>
                            @endforeach

                        </select>
                       
                    </div>
                    
                   
               
            </div>
            
                <div class="col-lg-12">
                    <button type="submit" class="btn btn-primary">Editar</button>
                </form>
                </div>





            </div>
        </div>

        

    </div>
</div>
</div>
</div>

  <script src="https://code.jquery.com/jquery-3.6.1.js" integrity="sha256-3zlB5s2uwoUzrXK3BT7AX3FyvojsraNFxCc2vC/7pNI=" crossorigin="anonymous"></script>
  <script>

    $("#form-edit").submit(function(event) {  //////////Se crea el evento cuando  se envia el formulario
        event.preventDefault();
        let formData = new FormData(document.getElementById("form-edit"));///////tomamos los valores del formulario
        alert('entor');
        $.ajax({ /////invocamos la funcion ajax
                type: "POST",
                url: "{!! route('appointment.edit') !!}", 
                /////usuamos las rutas de web que conecte la funcion editar de Usercontroler
                data: formData, ////enviamos los datos del formulario
                processData: false,
                contentType: false,
                success: (response) => {//////////si funciona nos mensaje de exito
                    alert("Citacion  editada con exito");
                    location.reload()
                  
                },
                error: (err) => {
                    alert('fallo');////si falla mensaje de fracaso
                }

    });

});
</script>
@endsection
