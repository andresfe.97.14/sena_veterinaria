@extends('layouts.app')

@section('content')
<div class="row">
    <div id='testt' class="container col-lg-12 col-md-12 col-sm-12" style="margin-bottom:60px;">

        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Citas ') }}</div>

                    <div class="card-body">
                        
                        @foreach($appointments_doctor as $appointment)



                        <h2>{{$appointment->date}}</h2>
                        <div class="card mb-3" style="max-width: 540px;">
                            <div class="row g-0">
                                <div class="col-md-4 col-sm-4">
                                    <img src="https://images.pexels.com/photos/5731866/pexels-photo-5731866.jpeg?cs=srgb&dl=pexels-sam-lion-5731866.jpg&fm=jpg" class="img-fluid rounded-start" style="height:140px;" alt="...">
                                </div>
                                <div class="col-md-8 col-sm-4">
                                    <div class="card-body">
                                        <h5 class="card-title">{{$appointment->type}}</h5>
                                        <p class="card-text">Mascota: {{$appointment->pet->name}} <br>
                                                Sede: {{$appointment->campus->name}} <br>
                                                Tipo: {{$appointment->type}}    <br>
                                                Observaciones: {{$appointment->observation}}

                                        </p>
                                        <p class="card-text"> </p>
                                        
                                        <a type="link"class="btn btn-success" href="{{ route('doctor.now',['pet' => $appointment->pet_id,
                                            'appointment' => $appointment->id ]) }}">Asistir</a>
                                        <button class="btn btn-danger"> Posponer</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach

                    </div>
                </div>
            </div>
        </div>
    </div>


</div>


<script src="https://code.jquery.com/jquery-3.6.1.js" integrity="sha256-3zlB5s2uwoUzrXK3BT7AX3FyvojsraNFxCc2vC/7pNI=" crossorigin="anonymous"></script>
<script>
   
   
</script>
@endsection
@section('scripts')



@endsection