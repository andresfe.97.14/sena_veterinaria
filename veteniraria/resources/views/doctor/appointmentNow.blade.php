@extends('layouts.app')

@section('content')

<div class="container rounded bg-white mt-5 mb-5" style="background-color:rgba(0, 0, 0, 0.5);">
    <div class="row">
        <div class="col-md-5 border-right">
            <div class="d-flex flex-column align-items-center text-center p-3 py-5">
                @if($pet->type == 'perro')
                <img src="https://images.pexels.com/photos/406014/pexels-photo-406014.jpeg?cs=srgb&dl=pexels-lumn-406014.jpg&fm=jpg" class="rounded-circle mt-10" alt="..." style="height:200px;">
                @elseif($pet->type == 'gato')
                <img src="https://images.pexels.com/photos/617278/pexels-photo-617278.jpeg?cs=srgb&dl=pexels-kelvin-valerio-617278.jpg&fm=jpg" class="rounded-circle mt-10" alt="..." style="height:200px;">
                @else
                <img src="https://images.pexels.com/photos/162240/bull-calf-heifer-ko-162240.jpeg?cs=srgb&dl=pexels-pixabay-162240.jpg&fm=jpg" class="rounded-circle mt-10" alt="..." style="height:200px;">
                @endif


                <br>
                <span class="font-weight-bold">Nombre: {{$pet->name}}</span>
                <span class="font-weight-bold">Raza: {{$pet->race}}</span>
                <span class="font-weight-bold">Genero: {{$pet->genre}}</span>
               
               
            </div>
        <!-- Se trae  los datos del usuario  con una imagen por defecto -->
        </div>
        <div class="col-md-7 border-right">
            <div class="p-3 py-5">
                <div class="d-flex justify-content-between align-items-center mb-3">
                    <h4 class="text-right">Cita Revision</h4>
                </div>
                <form id="form-edit">  <!--se le da un id al formulario para poder manipularlo con js -->
                @csrf   
                <div class="row mt-2">
                <input type="text" name="pet_id" id="pet_id"value="{{$pet->id}}" hidden >       
                <input type="text" name="appointment_id" id="appointment_id" value="{{$appointment->id}}" hidden >
                <label class="labels">Estado de la mascota</label>
                <textarea name="pet_state" id="pet_state" class="form-control" ></textarea>
                </div>

                <div class="row mt-2">
                <label class="labels">Recomendaciones</label>
                <textarea name="pet_recomend" id="pet_recomend" class="form-control" ></textarea>
                </div>

     
                <br>
                <div class="row mt-3">
                <div class="col-md-12   text-center"><button type="submit" class="btn btn-primary profile-button"  >Terminar revision</button></div>

               
                </form>
                
            </div>
        </div>

        

    </div>
</div>
</div>
</div>




  <!-- Button trigger modal -->


  <!-- Modal -->
  <div class="modal fade" id="uploadImage" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
            <div class="mb-3">
                <label for="formFile" class="form-label">Suba su imagen</label>
                <input class="form-control" type="file" id="formFile">
              </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary" onclick="uploadImage()">Save changes</button>
        </div>
      </div>
    </div>
  </div>
  <script src="https://code.jquery.com/jquery-3.6.1.js" integrity="sha256-3zlB5s2uwoUzrXK3BT7AX3FyvojsraNFxCc2vC/7pNI=" crossorigin="anonymous"></script>
  <script>

    $("#form-edit").submit(function(event) {  //////////Se crea el evento cuando  se envia el formulario
        event.preventDefault();
        let formData = new FormData(document.getElementById("form-edit"));///////tomamos los valores del formulario
        alert('entor');
        $.ajax({ /////invocamos la funcion ajax
                type: "POST",
                url: "{!! route('appointment.finish') !!}", 
                /////usuamos las rutas de web que conecte la funcion editar de Usercontroler
                data: formData, ////enviamos los datos del formulario
                processData: false,
                contentType: false,
                success: (response) => {//////////si funciona nos mensaje de exito
                    console.log(response);
                    alert("Usuario editado con exito");
                    location.href = '{!! route('doctor.index') !!}';
                },
                error: (err) => {
                    alert('fallo');////si falla mensaje de fracaso
                }

    });

});
</script>
@endsection
