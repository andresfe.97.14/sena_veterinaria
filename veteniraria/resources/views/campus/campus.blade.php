@extends('layouts.app')
@section('css')
<link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
@endsection

@section('content')
<div id='testt' class="container">

    <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
        Nuevo+
    </button>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Sedes') }}</div>

                <div class="card-body">
                    <div class="table-responsive">
                    <table class="table table-striped dt-responsive table-sm nowrap" style="width:100%" id="campus-table">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Nombre</th>
                                <th>Dirreccion</th>
                                <th class="text-center">Acciones</th>

                            </tr>
                        </thead>
                     
                    </table>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Crear nuevo campus</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form id="form-register">
                @csrf
                <div class="mb-3">
                        <label for="exampleInputPassword1" class="form-label">Nombre</label>
                        <input type="text" class="form-control" name="inputname"id="inputname" required>
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">Direccion</label>
                        <input type="text" class="form-control" id="inputaddress" name="inputaddress" aria-describedby="emailHelp" required>
                        <div id="emailHelp" class="form-text"></div>
                    </div>
                    
                   
                 
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Registrar</button>
                </form>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="exampleModal2" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form id="form-edit">
                @csrf   
                    <input type="text" id="editid" name="editid" hidden required>
                    <div class="mb-3">
                        <label for="exampleInputPassword1" class="form-label">Nombre</label>
                        <input type="text" class="form-control" name="editname"id="editname" required>
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">Direccion</label>
                        <input type="text" class="form-control" id="editaddress" name="editaddress" aria-describedby="emailHelp" required>
                        
                    </div>

                   
                   
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Editar</button>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="//code.jquery.com/jquery.js"></script>
        <!-- DataTables -->
        <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
        <!-- Bootstrap JavaScript -->
        <script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>



<script>
     $(function() {
            
            let table = $('#campus-table').DataTable({
                 processing: true,
                 serverSide: true,
                 responsive: true,
                 fixedColumns: true,
                 ajax: '{!! route('campus.table') !!}',
                
                 columns: [{
                         data: 'id',
                         name: 'id',
                         render: function (  data, type, row  ) {
                             return row.id;
                         }
                     },
                     {
                         data: 'name',
                         name: 'name',
                         render: function (  data, type, row  ) {
                             return row.name;
                         }
                     },
                     {
                         data: 'address',
                         name: 'address',
                         render: function (  data, type, row  ) {
                             return row.address;
                         }
                     },
                     {
                         data: null,
                         name: 'actions',
                         searchable: 'id',
                         orderable: 'id',
                         render: function (  data, type, row,index  ) {
                        
                           
                             return `
                               
                                     <button class="btn btn-danger " onclick='deleteCampus(${row.id})'  >
                                         <i class="fas fa-times"></i>Borrar
                                     </button>
                                     <button class="btn btn-success " onclick="editCampus(${row.id})"  data-bs-toggle="modal" data-bs-target="#exampleModal2">
                                         <i class="fas fa-pencil-alt"></i>Editar
                                     </button>
                
                             `;
                         }
                     },
                 ]
             });
 
             // s
         });





    $("#form-register").submit(function(event) {
        event.preventDefault();
        let formData = new FormData(document.getElementById("form-register"));
        formData.append("test", "funciona");
        
       
        $.ajax({
                type: "POST",
                url: "{!! route('campus.new') !!}",
                data: formData,
                processData: false,
                contentType: false,
                success: (response) => {
                    alert("Campus creado con exito");
                    location.reload();
                    if (response == 201) {
                        
                    
                    }
                },
                error: (err) => {
                    alert('fallo');
                }

    });

});

 function editCampus(id){
    
    let formData = new FormData();
            formData.append("_token", "{!! csrf_token() !!}");
            formData.append("id", id);

    $.ajax({
                type: "POST",
                url: "{!! route('campus.info') !!}",
                data: formData,
                processData: false,
                contentType: false,
                success: (response) => {
                    console.log(response);
                    alert("Usuario creado con exito1");
                    document.getElementById('editname').value = response.name;
                    document.getElementById('editid').value = id;
                    document.getElementById('editaddress').value = response.address;                
                },
                error: (err) => {
                    alert('fallo');
                }

    });
 }

 $("#form-edit").submit(function(event) {
        event.preventDefault();
        let formData = new FormData(document.getElementById("form-edit"));
        $.ajax({
                type: "POST",
                url: "{!! route('campus.edit') !!}",
                data: formData,
                processData: false,
                contentType: false,
                success: (response) => {
                    alert("Campus editado con exito");
                    location.reload();
                },
                error: (err) => {
                    alert('fallo');
                }
    });
});

function deleteCampus(id){
    
    let formData = new FormData();
            formData.append("_token", "{!! csrf_token() !!}");
            formData.append("id", id);
    $.ajax({
                type: "POST",
                url: "{!! route('campus.delete') !!}",
                data: formData,
                processData: false,
                contentType: false,
                success: (response) => {
                    console.log(response);
                    alert("Campus eliminado con exito");
                    location.reload();
                },
                error: (err) => {
                    alert('fallo');
                }

    });
 }


</script>
@endsection
@section('scripts')

@endsection