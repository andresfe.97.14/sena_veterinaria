@extends('layouts.app')

@section('css')
<link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
@endsection

@section('content')



<div id='testt' class="container">
    <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
        Nuevo+
    </button>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Usuarios') }}</div>

                <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped dt-responsive table-sm nowrap" style="width:100%" id="clients-table">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Nombre</th>
                                <th>email</th>
                              
                                <th class="text-center">Acciones</th>
                            </tr>
                        </thead>
                    </table>
                </div>

                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Crear nuevo usuario</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form id="form-register">
                @csrf
                <div class="mb-3">
                        <label for="exampleInputPassword1" class="form-label">Nombre</label>
                        <input type="text" class="form-control" name="inputname"id="inputname" required>
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">Email address</label>
                        <input type="email" class="form-control" id="exampleInputEmail1" name="exampleInputEmail1" aria-describedby="emailHelp" required>
                        <div id="emailHelp" class="form-text"></div>
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">Rol</label>
                        <select class="form-control" name="rol" id="rol" required> 
                            <option value="" disabled selected>Seleccione un rol</option>
                            @foreach($roles as $rol )
                                <option value="{{$rol->id}}">{{$rol->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputPassword1" class="form-label">Password</label>
                        <input type="password" class="form-control" id="exampleInputPassword1"  name="exampleInputPassword1" required>
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputPassword1" class="form-label">Password</label>
                        <input type="password" class="form-control" id="exampleInputPassword2"  name="exampleInputPassword2" required>
                    </div>
                   
                    
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn btn-primary">Registrar</button>
                </form>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="exampleModal2" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form id="form-edit">
                @csrf   
                    <input type="text" id="editid" name="editid" hidden required>
                <div class="mb-3">
                        <label for="exampleInputPassword1" class="form-label">Nombre</label>
                        <input type="text" class="form-control" name="editname"id="editname" required>
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">Email address</label>
                        <input type="email" class="form-control" id="editEmail" name="editEmail" aria-describedby="emailHelp" required>
                        <div id="emailHelp" class="form-text"></div>
                    </div>
                   
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>



 <!-- jQuery -->
 <script src="//code.jquery.com/jquery.js"></script>
        <!-- DataTables -->
        <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
        <!-- Bootstrap JavaScript -->
        <script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script>
     $(function() {
            
            $('#clients-table').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                fixedColumns: true,
                ajax: '{!! route('users.get') !!}',
               
                columns: [{
                        data: 'id',
                        name: 'id'
                    },
                    {
                        data: 'name',
                        name: 'name'
                    },
                    {
                        data: 'email',
                        name: 'email'
                    },
                   
                    {
                        data: null,
                        name: 'actions',
                        searchable: 'id',
                        orderable: 'id',
                        render: function (  data, type, row,index  ) {
                            console.log('=======')
                            console.log(data);
                            console.log('=======')
                            console.log('+++++++')
                            console.log(row);
                            console.log('+++++++')
                 
                          
                            return `
                              
                                    <button class="btn btn-danger " onclick='deleteUser(${row.id})'  >
                                        <i class="fas fa-times"></i>Borrar
                                    </button>
                                    <button class="btn btn-success " onclick="editUser(${row.id})"  data-bs-toggle="modal" data-bs-target="#exampleModal2">
                                        <i class="fas fa-pencil-alt"></i>Editar
                                    </button>
               
                            `;
                        }
                    },
                ]
            });

            // s
        });


    $("#form-register").submit(function(event) { /////se activa si se envia el formulario de registro
        event.preventDefault();
        let formData = new FormData(document.getElementById("form-register"));///guarda los valores del formulario
        formData.append("test", "funciona");
        
        $.ajax({
                type: "POST",
                url: "{!! route('users.new') !!}",////routa del Usercontroller para crear usuario
                data: formData, ////datos del formulario
                processData: false,
                contentType: false,
                success: (response) => {
                    alert("Usuario creado con exito");////si funciona mensaje de exito
                    location.reload();
                    if (response == 201) {
                        
                   
                    }
                },
                error: (err) => {
                    alert('fallo');
                    ////si falla mensaje de error
                }

    });

});

 function editUser(row){
   
    console.log(row);
    let formData = new FormData();
            formData.append("_token", "{!! csrf_token() !!}");
            formData.append("id", row);

    $.ajax({
                type: "POST",
                url: "{!! route('users.info') !!}",
                data: formData,
                processData: false,
                contentType: false,
                success: (response) => {
                    console.log(response);
                    alert("Usuario editado con exito");
                    document.getElementById('editname').value = response.name;
                    document.getElementById('editid').value = row;
                    document.getElementById('editEmail').value = response.email;
                    
                },
                error: (err) => {
                    alert('fallo');
                }

    });
 }

 $("#form-edit").submit(function(event) {///si se envia el formulario de ediccion se activa
        event.preventDefault();
        let formData = new FormData(document.getElementById("form-edit"));
        /////tomamos los valores del formulario
        $.ajax({
                type: "POST",
                url: "{!! route('users.edit') !!}",////funcion de editar del UserController
                data: formData, ///enviamos los datos del formulario
                processData: false,
                contentType: false,
                success: (response) => {
                    alert("Usuario editado con exito");
                    location.reload();
                    //////si funciona  mensaje de exito
                },
                error: (err) => {
                    alert('fallo');
                    ////si falla mensaje de erro
                }

    });

});



function deleteUser(id){
    console.log(id);
    let formData = new FormData();
            formData.append("_token", "{!! csrf_token() !!}");
            formData.append("id", id);////agregamos  el id del usuario para borrar

    $.ajax({
                type: "POST",
                url: "{!! route('users.delete') !!}", ////ruta de borrar usuario en UserController
                data: formData, //datos 
                processData: false,
                contentType: false,
                success: (response) => {
                    console.log(response);
                    alert("Usuario eliminado con exito");////si funciona mensaje de exito
                    location.reload();
                    
                },
                error: (err) => {
                    alert('fallo el eliminar usuario');////si falla error de fracaso
                }

    });
 }  

 let resources_actions = (row,index) => {
    console.log(row)
let  delete_action = `<button school_id="${row.id}" type="button" class="btn text-danger px-1 py-0 btn-delete-school"  data-bs-toggle="popover" data-bs-trigger="hover focus" data-bs-content="Disabled popover" ><i class="bi bi-trash3-fill"></i></button>`;
let  update_action = `<button index="${index}" type="button" class="btn text-primary px-1 py-0 btn-edit-school" ><i class="bi bi-pencil"></i></button>`;

 return `<div class="btn-group btn-group-lg"> ${delete_action} ${update_action}  </div>`;

}



</script>
@endsection
@section('scripts')



@endsection