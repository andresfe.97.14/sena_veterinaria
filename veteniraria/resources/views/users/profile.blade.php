@extends('layouts.app')

@section('content')

<div class="container rounded bg-white mt-5 mb-5" style="background-color:rgba(0, 0, 0, 0.5);">
    <div class="row">
        <div class="col-md-5 border-right">
            <div class="d-flex flex-column align-items-center text-center p-3 py-5">
                <img class="rounded-circle mt-5" width="150px" src="https://st3.depositphotos.com/15648834/17930/v/600/depositphotos_179308454-stock-illustration-unknown-person-silhouette-glasses-profile.jpg">
                <span class="font-weight-bold">{{$self_user->name}}</span>
                <span class="text-black-50">{{$self_user->email}}</span>
                <span> </span>
               
            </div>
        <!-- Se trae  los datos del usuario  con una imagen por defecto -->
        </div>
        <div class="col-md-7 border-right">
            <div class="p-3 py-5">
                <div class="d-flex justify-content-between align-items-center mb-3">
                    <h4 class="text-right">Perfil</h4>
                </div>
                <form id="form-edit">  <!--se le da un id al formulario para poder manipularlo con js -->
                @csrf   
                <input type="text" name="editid" id="editid" value="{{$self_user->id}}" hidden >
                <div class="row mt-2">
                    <div class="col-md-12">Nombre<label class="labels"></label><input type="text" class="form-control" name="editname" id="editname" placeholder="{{$self_user->name}}" value="{{$self_user->name}}"></div>

                </div>                
                <div class="row mt-3">
                    <div class="col-md-12"><label class="labels">Correo Electronico</label><input type="text" class="form-control" name="editEmail" id="editEmail" placeholder="{{$self_user->email}}" value="{{$self_user->email}}"></div>
                    <!-- <div class="col-md-12"><label class="labels">Telefono </label><input type="text" class="form-control" placeholder="{{$self_user->phone}}" value="{{$self_user->phone}}"></div> -->
                    <!-- <div class="col-md-12"><label class="labels">Numero de identificación</label><input type="text" class="form-control" placeholder="{{$self_user->nit}}" value=""></div> -->
                   
                </div>
                <br>
                <div class="row mt-3">
                <div class="col-md-6   text-center"><button type="submit" class="btn btn-primary profile-button"  >Guardar Cambios</button></div>

                <div class="col-md-6 text-center"> <a href="{{ route('user.pets') }}"  class="btn btn-success profile-button">Ver mascotas</a>  </div>
                </div> 
                </form>
                
            </div>
        </div>

        

    </div>
</div>
</div>
</div>




  <!-- Button trigger modal -->


  <!-- Modal -->
  <div class="modal fade" id="uploadImage" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
            <div class="mb-3">
                <label for="formFile" class="form-label">Suba su imagen</label>
                <input class="form-control" type="file" id="formFile">
              </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary" onclick="uploadImage()">Save changes</button>
        </div>
      </div>
    </div>
  </div>
  <script src="https://code.jquery.com/jquery-3.6.1.js" integrity="sha256-3zlB5s2uwoUzrXK3BT7AX3FyvojsraNFxCc2vC/7pNI=" crossorigin="anonymous"></script>
  <script>

    $("#form-edit").submit(function(event) {  //////////Se crea el evento cuando  se envia el formulario
        event.preventDefault();
        let formData = new FormData(document.getElementById("form-edit"));///////tomamos los valores del formulario
        alert('entor');
        $.ajax({ /////invocamos la funcion ajax
                type: "POST",
                url: "{!! route('users.edit') !!}", 
                /////usuamos las rutas de web que conecte la funcion editar de Usercontroler
                data: formData, ////enviamos los datos del formulario
                processData: false,
                contentType: false,
                success: (response) => {//////////si funciona nos mensaje de exito
                    alert("Usuario editado con exito");
                    location.reload()
                  
                },
                error: (err) => {
                    alert('fallo');////si falla mensaje de fracaso
                }

    });

});
</script>
@endsection
