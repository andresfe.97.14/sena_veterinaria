@extends('layouts.app')

@section('content')


<div class="container rounded bg-white mt-5 mb-5" style="background-color:rgba(0, 0, 0, 0.5);">

<button type="submit" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal" style="margin-bottom:30px;">Agregar Mascota</button>
    <div class="row">
        @foreach($pets_users as $pet_user)

        <div class="col-md-5 col-lg-4 border-right" style="margin-bottom:40px; margin-top:7px ;">

            <div class="card" style="width: 18rem;" >
                @if($pet_user->type == 'perro')
                <img src="https://images.pexels.com/photos/406014/pexels-photo-406014.jpeg?cs=srgb&dl=pexels-lumn-406014.jpg&fm=jpg" class="card-img-top" alt="..." style="height:200px;">
                @elseif($pet_user->type == 'gato')
                <img src="https://images.pexels.com/photos/617278/pexels-photo-617278.jpeg?cs=srgb&dl=pexels-kelvin-valerio-617278.jpg&fm=jpg" class="card-img-top" alt="..." style="height:200px;">
                @else
                <img src="https://images.pexels.com/photos/162240/bull-calf-heifer-ko-162240.jpeg?cs=srgb&dl=pexels-pixabay-162240.jpg&fm=jpg" class="card-img-top" alt="..." style="height:200px;">
                @endif
                <div class="card-body">
              
                    <h5 class="card-title">Nombre: {{$pet_user->name}}</h5>
                    <h5 class="card-text">Raza: {{$pet_user->race}}</h5>
                    <h5 class="card-text">Genero {{$pet_user->genre}} </h5>
                    <a href="#" class="btn btn-primary" onclick="editPet({{$pet_user->id}})"  data-bs-toggle="modal" data-bs-target="#exampleModal2"> 
                    Editar </a>  
                    <a href="{{ route('pet.appointments', ['pet' => $pet_user->id]) }}" class="btn btn-primary"> 
                    Historial </a>
                    <button  class="btn btn-danger" onclick='deleteUser({{$pet_user->id}})'>Borrar</button>
                </div>
            </div>
        </div>


        @endforeach
    </div>
</div>




<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Crear nueva mascota</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form id="form-register">
                    @csrf

                    <div class="mb-3">
                        <label for="exampleInputPassword1" class="form-label">Nombre</label>
                        <input type="text" class="form-control" name="inputiduser" id="inputiduser" value="{{$self_user->id}}" hidden>
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputPassword1" class="form-label">Nombre</label>
                        <input type="text" class="form-control" name="inputname" id="inputname" required>
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">Raza</label>
                        <input type="text" class="form-control" id="inputrace" name="inputrace" aria-describedby="emailHelp" required>
                        <div id="emailHelp" class="form-text"></div>
                    </div>

                    <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">Tipo</label>
                        <input type="text" class="form-control" id="inputtype" name="inputtype" aria-describedby="emailHelp" required>
                        <div id="emailHelp" class="form-text"></div>
                    </div>

                    <div class="mb-3">
                        <label for="exampleInputPassword1" class="form-label">Genero</label>
                        <input type="text" class="form-control" id="inputgenre" name="inputgenre" required>
                    </div>

                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>



<!-- Modal -->
<div class="modal fade" id="exampleModal2" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form id="form-edit">
                @csrf   
                    <input type="text" id="editid" name="editid" hidden required>
                    <div class="mb-3">
                        <label for="exampleInputPassword1" class="form-label">Nombre</label>
                        <input type="text" class="form-control" name="editname"id="editname" required>
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">Raza</label>
                        <input type="text" class="form-control" id="editrace" name="editrace" aria-describedby="emailHelp" required>
                        <div id="emailHelp" class="form-text"></div>
                    </div>

                    <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">Tipo</label>
                        <input type="text" class="form-control" id="edittype" name="edittype" aria-describedby="emailHelp" required>
                        <div id="emailHelp" class="form-text"></div>
                    </div>
                    
                    


                    </div>
                    <div class="mb-3">
                        <label for="exampleInputPassword1" class="form-label">Genero</label>
                        <input type="text" class="form-control" id="editgenre"  name="editgenre" required>
                    </div>
                   
                   
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn btn-primary">Editar</button>
                
            </div>
        </div>
    </div>
</div>

<!-- Button trigger modal -->


<!-- Modal -->
<div class="modal fade" id="uploadImage" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="mb-3">
                    <label for="formFile" class="form-label">Suba su imagen</label>
                    <input class="form-control" type="file" id="formFile">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" onclick="uploadImage()">Save changes</button>
            </div>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.6.1.js" integrity="sha256-3zlB5s2uwoUzrXK3BT7AX3FyvojsraNFxCc2vC/7pNI=" crossorigin="anonymous"></script>
<script>
    $("#form-register").submit(function(event) {
        event.preventDefault();
        let formData = new FormData(document.getElementById("form-register"));
        alert('entor');
        $.ajax({
            type: "POST",
            url: "{!! route('pet.create') !!}",
            data: formData,
            processData: false,
            contentType: false,
            success: (response) => {
                alert("Mascota asociada con exito");
                location.reload()

            },
            error: (err) => {
                alert('fallo');
            }

        });

    });

    let deleteUser = (id) => {
        alert('entro');
        alert(id);

        let formData = new FormData();
            formData.append("_token", "{!! csrf_token() !!}");
            formData.append("id", id);////agregamos  el id del usuario para borrar

            $.ajax({
                type: "POST",
                url: "{!! route('pet.delete') !!}", ////ruta de borrar usuario en UserController
                data: formData, //datos 
                processData: false,
                contentType: false,
                success: (response) => {
                    console.log(response);
                    alert("Mascota eliminada con exito");////si funciona mensaje de exito

                    location. reload()
                  
                    
                },
                error: (err) => {
                    alert('fallo');////si falla error de fracaso
                }

    });
    }


    
 function editPet(id){    
    alert("holsls");
    let formData = new FormData();
            formData.append("_token", "{!! csrf_token() !!}");
            formData.append("id", id);

    $.ajax({
                type: "POST",
                url: "{!! route('pet.info') !!}",
                data: formData,
                processData: false,
                contentType: false,
                success: (response) => {
                    console.log(response);




                    document.getElementById('editname').value = response.pet.name;
                    document.getElementById('editid').value = id;
                    document.getElementById('editrace').value = response.pet.race;
                    document.getElementById('edittype').value = response.pet.type;
                    
                    document.getElementById('editgenre').value = response.pet.genre; 


                    
                    
                   

                },
                error: (err) => {
                    alert('fallo');
                }

    });
 }

 
 $("#form-edit").submit(function(event) {
  
        event.preventDefault();
        let formData = new FormData(document.getElementById("form-edit"));
 
        $.ajax({
                type: "POST",
                url: "{!! route('pet.edit') !!}",
                data: formData,
                processData: false,
                contentType: false,
                success: (response) => {
                    alert("Mascota editada con exito");
                    location.reload();
                },
                error: (err) => {
                    alert('fallo');
                }

    });

});

</script>
@endsection