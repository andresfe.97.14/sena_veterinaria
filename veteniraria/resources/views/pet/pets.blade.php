@extends('layouts.app')

@section('css')
<link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
@endsection

@section('content')
<div id='testt' class="container">

    <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
        Nuevo+
    </button>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Mascotas') }}</div>

                <div class="card-body">
                    <div class="table-responsive">
                    <table class="table table-striped dt-responsive table-sm nowrap" style="width:100%" id="pets-table">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Nombre</th>
                                <th>Raza</th>
                                <th>Tipo</th>
                                <th>Genero</th>
                                <th>Resposable</th>
                                <th class="text-center">Acciones</th>

                            </tr>
                        </thead>
                     
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Crear nueva mascota</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form id="form-register">
                @csrf
                <div class="mb-3">
                        <label for="exampleInputPassword1" class="form-label">Nombre</label>
                        <input type="text" class="form-control" name="inputname"id="inputname" required>
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">Raza</label>
                        <input type="text" class="form-control" id="inputrace" name="inputrace" aria-describedby="emailHelp" required>
                        <div id="emailHelp" class="form-text"></div>
                    </div>

                    <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">Tipo</label>
                        <input type="text" class="form-control" id="inputtype" name="inputtype" aria-describedby="emailHelp" required>
                        <div id="emailHelp" class="form-text"></div>
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputPassword1" class="form-label">Responsable</label>
                        <select class="form-control" id="inputuser"  name="inputuser" required>
                            <option value="" disabled selected>Seleccione un usuario </option>
                            @foreach($users as $user)
                                <option value="{{$user->id}}">{{$user->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputPassword1" class="form-label">Genero</label>
                        <input type="text" class="form-control" id="inputgenre"  name="inputgenre" required>
                    </div>
                   
                 
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn btn-primary">Registrar</button>
                </form>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="exampleModal2" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form id="form-edit">
                @csrf   
                    <input type="text" id="editid" name="editid" hidden required>
                    <div class="mb-3">
                        <label for="exampleInputPassword1" class="form-label">Nombre</label>
                        <input type="text" class="form-control" name="editname"id="editname" required>
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">Raza</label>
                        <input type="text" class="form-control" id="editrace" name="editrace" aria-describedby="emailHelp" required>
                        <div id="emailHelp" class="form-text"></div>
                    </div>

                    <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">Tipo</label>
                        <input type="text" class="form-control" id="edittype" name="edittype" aria-describedby="emailHelp" required>
                        <div id="emailHelp" class="form-text"></div>
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputPassword1" class="form-label">Responsable</label>
                        <select class="form-control" id="edituser"  name="edituser" required>
                            <option value="" disabled selected>Seleccione un usuario </option>
                            @foreach($users as $user)
                                <option value="{{$user->id}}">{{$user->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div id="div-user" class="mb-3">


                    </div>
                    <div class="mb-3">
                        <label for="exampleInputPassword1" class="form-label">Genero</label>
                        <input type="text" class="form-control" id="editgenre"  name="editgenre" required>
                    </div>
                   
                   
           
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn btn-primary">Editar</button>
                </form>
            </div>
        </div>
    </div>
</div>

 <!-- jQuery -->
 <script src="//code.jquery.com/jquery.js"></script>
        <!-- DataTables -->
        <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
        <!-- Bootstrap JavaScript -->
        <script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script>


$(function() {
            
            let table = $('#pets-table').DataTable({
                 processing: true,
                 serverSide: true,
                 responsive: true,
                 fixedColumns: true,
                 ajax: '{!! route('pet.table') !!}',
                
                 columns: [{
                         data: 'id',
                         name: 'id',
                         render: function (  data, type, row  ) {
                             return row.id;
                         }
                     },
                     {
                         data: 'name',
                         name: 'name',
                         render: function (  data, type, row  ) {
                             return row.name;
                         }
                     },
                     {
                         data: 'race',
                         name: 'race',
                         render: function (  data, type, row  ) {
                             return row.race;
                         }
                     },
                     {
                         data: 'type',
                         name: 'type',
                         render: function (  data, type, row  ) {
                             return row.type;
                         }
                     },
                     {
                         data: 'genre',
                         name: 'genre',
                         render: function (  data, type, row  ) {
                             return row.genre;
                         }
                     },
                     {
                         data: 'user',
                         name: 'user',
                         render: function (  data, type, row  ) {
                             return row.user.name;
                         }
                     },
                     
                     {
                         data: null,
                         name: 'actions',
                         searchable: 'id',
                         orderable: 'id',
                         render: function (  data, type, row,index  ) {
                        
                           
                             return `
                               
                                     <button class="btn btn-danger " onclick='deletePet(${row.id})'  >
                                         <i class="fas fa-times"></i>Borrar
                                     </button>
                                     <button class="btn btn-success " onclick="editPet(${row.id})"  data-bs-toggle="modal" data-bs-target="#exampleModal2">
                                         <i class="fas fa-pencil-alt"></i>Editar
                                     </button>
                
                             `;
                         }
                     },
                 ]
             });
 
             // s
         });


    $("#form-register").submit(function(event) {
        event.preventDefault();
        let formData = new FormData(document.getElementById("form-register"));
        formData.append("test", "funciona");
        
       
        $.ajax({
                type: "POST",
                url: "{!! route('pets.new') !!}",
                data: formData,
                processData: false,
                contentType: false,
                success: (response) => {
                    alert("Mascota creado con exito");
                    location.reload();
                    if (response == 201) {
                        
                        
                   
                    }
                },
                error: (err) => {
                    alert('fallo');
                }

    });

});

 function editPet(id){    
    let formData = new FormData();
            formData.append("_token", "{!! csrf_token() !!}");
            formData.append("id", id);

    $.ajax({
                type: "POST",
                url: "{!! route('pet.info') !!}",
                data: formData,
                processData: false,
                contentType: false,
                success: (response) => {
                    console.log(response);




                    document.getElementById('editname').value = response.pet.name;
                    document.getElementById('editid').value = id;
                    document.getElementById('editrace').value = response.pet.race;
                    document.getElementById('edittype').value = response.pet.type;
                    document.getElementById('edituser').value = response.pet.user_id;
                    document.getElementById('editgenre').value = response.pet.genre; 


                    let div_select_user = document.getElementById('div-user');
                    
                   

                },
                error: (err) => {
                    alert('fallo');
                }

    });
 }

 $("#form-edit").submit(function(event) {
        event.preventDefault();
        let formData = new FormData(document.getElementById("form-edit"));
 
        $.ajax({
                type: "POST",
                url: "{!! route('pet.edit') !!}",
                data: formData,
                processData: false,
                contentType: false,
                success: (response) => {
                    alert("Mascota editada con exito");
                    location.reload();
                },
                error: (err) => {
                    alert('fallo');
                }

    });

});



function deletePet(id){
    
    let formData = new FormData();
            formData.append("_token", "{!! csrf_token() !!}");
            formData.append("id", id);

    $.ajax({
                type: "POST",
                url: "{!! route('pet.delete') !!}",
                data: formData,
                processData: false,
                contentType: false,
                success: (response) => {
                    console.log(response);
                    alert("Mascota eliminada con exito");
                    location.reload();
                  
                    
                },
                error: (err) => {
                    alert('Error al eliminar mascota');
                }

    });
 }


</script>
@endsection
@section('scripts')



@endsection