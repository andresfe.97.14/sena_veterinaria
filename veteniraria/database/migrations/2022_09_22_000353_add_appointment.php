





<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;


return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appointments', function(Blueprint $table){
            $table->id();
            $table->dateTime('date');
            $table->string('hour');
            $table->string('type');
            $table->string('observation');
            $table->boolean('state');
            $table->unsignedBigInteger('doctor_id');
            $table->unsignedBigInteger('pet_id');
            $table->string('campus_id')->nullable()->references('id')->on('campus');
            $table->foreign('doctor_id')->references('id')->on('users');
            $table->foreign('pet_id')->references('id')->on('pets');
            $table->timestamps();
            

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
};
