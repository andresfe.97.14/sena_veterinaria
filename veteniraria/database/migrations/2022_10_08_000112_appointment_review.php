<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appointment_reviews', function(Blueprint $table){
            $table->id();
            $table->string('state_pet');
            $table->string('recommendations');
            $table->foreignId('appointment_id')->constrained('appointments');
            $table->foreignId('pet_id')->constrained('pets');
            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
};
