<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

use App\Models\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class AadRolesToTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      

        $createPacientUser = Permission::create(['name'=> 'create_user']);
        $giveDate = Permission::create(['name'=> 'give_date']);
        $associates_pet = Permission::create(['name' => 'associated_pet']);
        $create_date = Permission::create(['name' => 'create_date']);
        $edit_date = Permission::create(['name' => 'edit_date']);
        $edit_user = Permission::create(['name' => 'edit_user']);
        $edit_pet = Permission::create(['name' => 'edit_pet']);


        $superAdmin = Role::create(['name' => 'superAdmin']);
        $doctor = Role::create(['name' => 'doctor']);
        $patient = Role::create(['name' => 'patient']);

        ///Add default Permissions 

        $superAdmin->givePermissionTo(Permission::all());
        $doctor->givePermissionTo($edit_pet, $giveDate);
        $patient->givePermissionTo($edit_user, $create_date, $edit_date);

        //create  User with rol    
        $userAdmin = User::create(['name' => 'administrador', 'email' => 'admin@email.com', 'password' => Hash::make('123456789')]);
        $userdoctor = User::create(['name' => 'doctor', 'email' => 'doctor@email.com', 'password' => Hash::make('123456789')]);
        $userPatient = User::create(['name' => 'paciente',  'email' => 'paciente@email.com', 'password' => Hash::make('123456789')]);

        $userAdmin->assignRole($superAdmin);
        $userdoctor->assignRole($doctor);
        $userPatient->assignRole($patient);


    }
}
