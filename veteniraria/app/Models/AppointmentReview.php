<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AppointmentReview extends Model
{
    use HasFactory;

    protected $fillable = [
        'state_pet',
        'recommendations',
        'appointment_id',
        'pet_id',
    ];

    public function pet(){
        return $this->belongsTo(Pet::class);
    }

    public function appointment(){
        return $this->belongsTo(Appointment::class);
    }
}
