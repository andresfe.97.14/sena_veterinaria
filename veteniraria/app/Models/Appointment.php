<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
    use HasFactory;

    protected $fillable = [
        'date',
        'hour',
        'doctor_id',
        'state',
        'type',
        'observation',
        'pet_id',
        'campus_id'
    ];


    public function doctor(){
         return $this->belongsTo(User::class);         
    }
    public function pet(){
        return $this->belongsTo(Pet::class); 
    }

    public function campus(){
        return $this->belongsTo(Campus::class); 
    }
}
