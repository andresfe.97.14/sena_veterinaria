<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pet extends Model
{
    use HasFactory, SoftDeletes;
    
    protected $fillable = [
        'name',
        'type',
        'race',
        'genre',
        'user_id'
    ];

    public function user(){
        return $this->belongsTo(User::class);
        // Aca se llama la funcion  user para decir que la llave foranea es 
        // User  y se commo  se llama el Model User  con su clase
    }
}
