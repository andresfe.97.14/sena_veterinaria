<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Appointment;
use App\Models\User;
use App\Models\Pet;
use App\Models\Campus;
use App\Models\AppointmentReview;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
use Yajra\DataTables\DataTables;
use Carbon\Carbon;

class AppointmentController extends Controller
{
    public function index(){
        $appointments = Appointment::all();
        $users_doctor = User::role('doctor')->get();
        $pets = Pet::all();
        $campuses = Campus::all();
        // dd($users);
        return view('appointments.index', compact('appointments', 'users_doctor', 'pets', 'campuses'));
    }

    public function getTable(){

        return DataTables::of(Appointment::query()->with(['doctor', 'pet', 'campus']))->make(true);

    }



    public function new(Request $request){
        debug('holas');
        debug($request->all());
        $date1 = Carbon::now();
        debug($date1->format('d-m-Y'));
        $date = Carbon::parse($request->inputdate);
        debug($date->format('d-m-Y'));
        

        $data = [
            'date' => $date->format('d-m-Y'),
            'hour' => $request->inputhour,
            'type' => $request->inputtype,
            'observation' => $request->inputobservation,
            'state' => true,
            'doctor_id' => $request->inputdoctorid,
            'pet_id' => $request->inputpetid,
            'campus_id' => $request->inputcampusid
                   ];
        debug($data);
        Appointment::create($data);
        
    }

    public function getAppointment (Request $request){
        $appointment = Appointment::find($request->id);
        $doctor = User::role('doctor')->get();
        $pets = Pet::all();
        $campus = Campus::all();

        $info = [
            'apointments' => $appointment,
            'doctors' => $doctor,
            'pets' => $pets,
            'campus' => $campus
        ];
        debug($appointment);
        return response($info);
    }

    public function editAppointment(Request $request){
        debug($request->all());
        $appointment_edit = Appointment::find($request->editid);
        debug($appointment_edit);
        debug($request->editdoctorid);
       
        $appointment_edit->date = $request->editdate;
        $appointment_edit->hour = $request->edithour;
        $appointment_edit->type = $request->edittype;
        $appointment_edit->observation = $request->editobservation;
        $appointment_edit->doctor_id = $request->editdoctorid;
        $appointment_edit->pet_id = $request->editpetid;
        $appointment_edit->campus_id = $request->editcampusid;

        $appointment_edit->save();

    }

    public function delete( Request $request){

        $appointment = Appointment::find($request->id);
        debug($appointment);
        $appointment->delete();


        debug($appointment);
    }

    public function finish(Request $request){

        debug($request->all());

        try{
        DB::beginTransaction();
        debug('holala');    
        $data = [
            'state_pet' =>  $request->pet_state,
            'recommendations' => $request->pet_recomend,
            'appointment_id' => $request->appointment_id,
            'pet_id' => $request->pet_id
        ];
        debug($data);
         $appointment_response = AppointmentReview::create($data); 

         $appointment_off = Appointment::find($request->appointment_id);
         $appointment_off->state = false;
         $appointment_off->save();
         DB::commit();
        debug($appointment_response);
       
        return response('paso',200);
        
        }catch(\Throwable $th){
            DB::rollBack();
            dd($th);
        
        debug('fallo');
        return response()->json(Response::HTTP_BAD_REQUEST);
        }

        
    }

    public function appointmentNow($id){
     

        $appointment_now = Appointment::where('id', $id)->with(['doctor', 'pet', 'campus'])->first();

        $campuses = Campus::all();
        $pets = Pet::all();
        $users_doctor = User::role('doctor')->get();
      
         return view('appointments.appointmentView', compact('appointment_now', 'campuses', 'pets', 'users_doctor'));

    }


    public function reviewAppointmentPet(Pet $pet){

        $appointmentPet = AppointmentReview::where('pet_id', $pet->id)->get();

        return view('pet.historyappoinment', compact('appointmentPet', "pet"));

    }
}
