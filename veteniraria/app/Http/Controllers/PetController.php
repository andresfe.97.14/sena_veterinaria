<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pet;
use App\Models\User;
use Yajra\DataTables\DataTables;

class PetController extends Controller
{
    public function index(){
         ////Aca se llama el modelo  y se le pide  todo los datos que esten en la tabla
        $pets = Pet::with('user')->get();
        $users = User::all();
        return view('pet.pets', compact('pets', 'users'));
        ////Aca se retorna  la vista y como dato se le envia los datos de user

    }

    public function new(Request $request){

        debug($request->all());
        //// el request  trae toda la informacion que se recolecto antes de llamar
        //// la funcion
        $data = [
            'name' => $request->inputname,
            'race' => $request->inputrace,
            'type' => $request->inputtype,
            'user_id' => $request->inputuser,
            'genre' => $request->inputgenre            
        ];
        Pet::create($data);
        //////se llama el mdelo para crear una mascota nuevo con el obj anteriormente creado
        
    }

    public function getPet (Request $request){
        ////// se  busca  una mascota   en base al identificador unico de la mascota a editar
        $pet = Pet::find($request->id);
         /// se trae toda la informacion y retorna el objeto mascota
        $users = User::all();

        $data = ['pet' => $pet,
                 'users' => $users   ];
        return($data);

    }

    public function editPet (Request $request){
        debug($request->all());
        ////// se  busca  una mascota   en base al identificador unico de la mascota a editar
        $pet_edit = Pet::find($request->editid);
        //////se ediita cada dato del modelo mascota
        $pet_edit->name = $request->editname;
        $pet_edit->race = $request->editrace;
        if(isset($request->edituser)){
            $pet_edit->user_id = $request->edituser;
        }
        $pet_edit->type = $request->edittype;
        
        $pet_edit->genre = $request->editgenre;
        // se guardan los datos editados
        $pet_edit->save();

    }

    public function delete( Request $request){
        ////// se  busca  una mascota   en base al identificador u   nico de la mascoa a editar
        $pet = Pet::find($request->id);
        debug($pet);
        ///// con la mascota encontrado,  se elimina
        $pet->delete();
    }


    public function createPetUser(Request $request){
        debug($request->all());
        $datapet =[
            'name'=> $request->inputname,
            'type' => $request->inputtype,
            'race' => $request->inputrace,
            'genre' => $request->inputgenre,
            'user_id' => $request->inputiduser
        ];
        debug($datapet);
        $pet = Pet::create($datapet);
        debug($pet);
    }


    public function getTable (){
        return DataTables::of(Pet::query()->with(['user']))->make(true);
    }
}
