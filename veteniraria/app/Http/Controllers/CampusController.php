<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Campus;
use Yajra\DataTables\DataTables;

class CampusController extends Controller
{
     public function index(){
        ////Aca se llama el modelo  y se le pide  todo los datos que esten en la tabla
        $campuses = Campus::all();
        // dd($users);
        return view('campus.campus', compact('campuses'));
         ////Aca se retorna  la vista y como dato se le envia los datos de campuses
    }

    public function new(Request $request){
        debug($request->all());
        //// el request  trae toda la informacion que se recolecto antes de llamar
        //// la funcion 
        $data = [
            'name' => $request->inputname,
            'address' => $request->inputaddress,
                   ];
        ///// se hace un objeto  con los datos  del campus
        Campus::create($data);
        //////se llama el mdelo para crear un campus nuevo con el obj anteriormente creado        
    }

    public function getCampusInfo (Request $request){
         ////// se  busca  un campus en base al identificador unico del campus a editar
        $campus = Campus::find($request->id);
        /// se trae toda la informacion y retorna el objeto campus
        return($campus);
    }

    public function editCampus (Request $request){
        debug($request->all());
        ////// se  busca  un campus   en base al identificador unico del campus a editar
        $campus_edit = Campus::find($request->editid);
         //////se ediita cada dato del modelo Campus
        $campus_edit->name = $request->editname;
        $campus_edit->address = $request->editaddress;
        // se guardan los datos editados
        $campus_edit->save();
    }

    public function delete( Request $request){
        ////// se  busca  un campus en base al identificador unico del campus a editar
        $campus = Campus::find($request->id);
        ///// con el campus encontrado,  se elimina
        $campus->delete();
    }

    public function getTable (){
        return DataTables::of(Campus::query())->make(true);
    }
}
