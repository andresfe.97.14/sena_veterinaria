<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pet;
use App\Models\User;
use App\Models\PetUser;
use App\Models\Appointment;

use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;
use Carbon\Carbon;



class DoctorController extends Controller
{
    public function index(){
        $date = Carbon::now();
        // dd($date->format('Y'));
        


        $doctor = Auth::User(); 
        $appointments_doctor = Appointment::where('doctor_id',  $doctor->id)->where('state', true)->with(['pet','campus'])->get();
        
        ////Aca se llama el modelo  y se le pide  todo los datos que esten en la tabla
        return view('doctor.homeDoctor', compact('doctor','appointments_doctor')); 
        ////Aca se retorna  la vista y como dato se le envia los datos de user


    }

    public function appointmentNow(Request $request, Pet $pet, Appointment $appointment){
        return view('doctor.appointmentNow', compact('pet', 'appointment'));

    }
    public function get_users()
    {   
        //// Se exporta los datatables con el modelo que quiere y las restricciones 
        ///  que se le pida al modelo
        return DataTables::of(User::query())->make(true);
    }


    public function newUser(Request $request){
        debug($request->all());
        //// el request  trae toda la informacion que se recolecto antes de llamar
        //// la funcion 
        $data = [
            'name' => $request->inputname,
            'email' => $request->exampleInputEmail1,
            'password' => bcrypt($request->exampleInputPassword1)
        ];///// se hace un objeto  con los datos  del usuario

        User::create($data);
        //////se llama el mdelo para crear un usuario nuevo con el obj anteriormente creado
    }

    public function getUserInfo (Request $request){
        ////// se  busca  un usuario   en base al identificador unico del usuario a editar
        debug($request->all());
        
        debug(json_decode($request->id));
        $user = User::find($request->id);
        /// se trae toda la informacion y retorna el objeto usuario
        return($user);

    }

    public function editUser (Request $request){
        debug($request->all());
        ////// se  busca  un usuario   en base al identificador unico del usuario a editar
        $user_edit = User::find($request->editid);

        //////se ediita cada dato del modelo Usuario
        $user_edit->name = $request->editname;
        $user_edit->email = $request->editEmail;
        // se guardan los datos editados
        $user_edit->save();

    }

    public function delete( Request $request){
        ////// se  busca  un usuario   en base al identificador u   nico del usuario a editar
        $user = User::find($request->id);
        debug($user);
        ///// con el usuario encontrado,  se elimina
        $user->delete();
        
    }

    public function userProfile(){
        /////la funcion Auth:: trae la informacion del usuarrio logeado
        $self_user = Auth::User();
        ////// con id del usuario autentificado se busca las mascotas asociados
        $pets_users = Pet::where('user_id', $self_user->id)->get();
        // se retorna la visata a companada de  las variables
        return view('users.profile', compact('self_user', 'pets_users'));
    }

    public function userPet(){
        /////la funcion Auth:: trae la informacion del usuarrio logeado
        $self_user = Auth::User();

        ////// con id del usuario autentificado se busca las mascotas asociados
        $pets_users = Pet::where('user_id', $self_user->id)->get();
       
     
       // se retorna la visata a companada de  las variables
        return view('users.petUser', compact('self_user', 'pets_users'));
    
    }
}
